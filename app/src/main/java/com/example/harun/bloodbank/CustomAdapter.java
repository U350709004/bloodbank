package com.example.harun.bloodbank;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CustomAdapter extends BaseAdapter {
    LayoutInflater layoutInflater;
    List<WantedBlood> wantedBloodList;
    Activity activity;
    public CustomAdapter(Activity activity, List<WantedBlood> wList){
        layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        wantedBloodList =wList;
        this.activity =activity;
    }
    @Override
    public int getCount() {
        return wantedBloodList.size();
    }



    @Override
    public Object getItem(int position) {
        return wantedBloodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View satirView;
        satirView = layoutInflater.inflate(R.layout.main_page_custom_list_view,null);
        TextView fNamePhone = (TextView)satirView.findViewById(R.id.fNamePhone);
        TextView blood = (TextView)satirView.findViewById(R.id.blood);
        TextView city  = (TextView)satirView.findViewById(R.id.city);
        TextView town = (TextView)satirView.findViewById(R.id.town);

        final WantedBlood wb = wantedBloodList.get(position);
        fNamePhone.setText(wb.getFullName().toString()+" ( " +wb.getPhoneNumber()+" ) ");
        blood.setText(wb.getBlood().toString());
        city.setText(wb.getCity().toString());
        town.setText(wb.getTown().toString());
        satirView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String numb = "tel:"+ wb.getPhoneNumber().toString();
                satirView.getContext().startActivity(new Intent(Intent.ACTION_DIAL,Uri.parse(numb)));
            }
        });
        return satirView;
    }
}