package com.example.harun.bloodbank;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.example.harun.bloodbank.fragments.FragmentEditProfile;
import com.example.harun.bloodbank.fragments.FragmentFindingBlood;
import com.example.harun.bloodbank.fragments.FragmentProfile;
import com.example.harun.bloodbank.fragments.FragmentProfileFindsFlood;
import com.example.harun.bloodbank.interfaces.LoginInterface;
import com.example.harun.bloodbank.interfaces.ProfileInterface;

public class Profile extends AppCompatActivity implements ProfileInterface, FragmentManager.OnBackStackChangedListener {
    FragmentManager manager;
    FragmentTransaction transaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        FragmentProfile fp = new FragmentProfile();

        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.container,fp);

        transaction.commit();

    }



    @Override
    public void replaceFragment(String target) {

        transaction = manager.beginTransaction();
        if(target == String.valueOf("editProfile")) {
            FragmentEditProfile fep = new FragmentEditProfile();
            transaction.addToBackStack("fragEP");
            transaction.replace(R.id.container,fep);
        }else if(target == String.valueOf("profile")) {
            FragmentProfile fp = new FragmentProfile();
            transaction.replace(R.id.container,fp);
        }else if(target == String.valueOf("findingBlood")) {
            FragmentProfileFindsFlood fpff = new FragmentProfileFindsFlood();
            transaction.addToBackStack("fragFPFF");
            transaction.replace(R.id.container,fpff);
        }


        transaction.commit();
    }



    @Override
    public void onBackStackChanged() {

    }
}
