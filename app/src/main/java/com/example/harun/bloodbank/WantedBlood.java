package com.example.harun.bloodbank;


public class WantedBlood {
    private String FullName;
    private String PhoneNumber;
    private String City;
    private String Town;
    private String Blood;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    private int ID;
    public WantedBlood(String fullName,String phoneNumber, String city,String town, String blood){
        FullName = fullName;
        PhoneNumber = phoneNumber;
        City = city;
        Town = town;
        Blood =blood;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String town) {
        Town = town;
    }

    public String getBlood() {
        return Blood;
    }

    public void setBlood(String blood) {
        Blood = blood;
    }


    @Override
    public String toString() {
        return this.getFullName()+" "+getBlood().toString();
    }
}
