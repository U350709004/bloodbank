package com.example.harun.bloodbank.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.WantedBlood;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {
    LayoutInflater inflater;
    List<WantedBlood> wantedBloodList;
    Context context;
    public MainAdapter (Context context,List<WantedBlood> wList) {
        //inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.wantedBloodList = wList;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = inflater.inflate(R.layout.main_page_custom_list_view,viewGroup,false);
        MyViewHolder holder = new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        WantedBlood wbl = wantedBloodList.get(i);
        myViewHolder.setData(wbl,i);
        final WantedBlood wb = wbl;
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String numb = "tel:"+ wb.getPhoneNumber().toString();
                myViewHolder.itemView.getContext().startActivity(new Intent(Intent.ACTION_DIAL,Uri.parse(numb)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return wantedBloodList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView fNamePhone,blood,city,town,phoneNumber;

        public MyViewHolder(@NonNull View satirView) {
            super(satirView);


            //satirView = inflater.inflate(R.layout.main_page_custom_list_view,null);
            fNamePhone = (TextView)satirView.findViewById(R.id.fNamePhone);
            blood = (TextView)satirView.findViewById(R.id.blood);
            city  = (TextView)satirView.findViewById(R.id.city);
            town = (TextView)satirView.findViewById(R.id.town);



        }

        public void setData(WantedBlood wb,int position ){
            this.fNamePhone.setText(wb.getFullName().toString()+" ( " +wb.getPhoneNumber()+" ) ");

            this.blood.setText(wb.getBlood().toString());
            this.city.setText(wb.getCity().toString());
            this.town.setText(wb.getTown().toString());
        }
    }
}
