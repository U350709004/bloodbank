package com.example.harun.bloodbank.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.WantedBlood;
import com.example.harun.bloodbank.database.Database;
import com.example.harun.bloodbank.fragments.FragmentProfileFindsFlood;

import java.util.ArrayList;
import java.util.List;

public class MyFindingBloodAdapter extends RecyclerView.Adapter<MyFindingBloodAdapter.MyViewHolder>{

    ArrayList<WantedBlood> mDataList;
    LayoutInflater inflater;
    Context  context;
    public MyFindingBloodAdapter(Context context, List<WantedBlood> data){
        this.context = context;
        //inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater=LayoutInflater.from(context);

        this.mDataList= (ArrayList<WantedBlood>) data;


    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v=inflater.inflate(R.layout.finding_blood_item, parent,false);
        MyViewHolder holder=new MyViewHolder(v);
        return holder;

    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {



        WantedBlood tiklanilanKan=mDataList.get(position);
        holder.setData(tiklanilanKan, position);

    }

    public void deleteItem(int position,int ogeId){
        mDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,mDataList.size());
        Database db = new Database(context);
        db.deletItem(ogeId);

        //Aşağıdaki method yukarıdaki iki satır kodun yaptığı işi yapar,
        //Ama çok kaynak tüketilir. ve de animasyonları göremeyiz.
        //notifyDataSetChanged();
    }

    public void addItem(int position, WantedBlood kopyalanacakManzara){
        mDataList.add(position, kopyalanacakManzara);
        notifyItemInserted(position);
        notifyItemRangeChanged(position,mDataList.size());

        //Aşağıdaki method yukarıdaki iki satır kodun yaptığı işi yapar,
        //Ama çok kaynak tüketilir. ve de animasyonları göremeyiz.
        //notifyDataSetChanged();
    }



    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView mKan, mYer;
        ImageView mSilResmi;
        int tiklanilanOgeninPositionDegeri=0;
        int tiklanilanOgeId;
        WantedBlood kopyalanacakKan;


        public MyViewHolder(View itemView) {
            super(itemView);

            mKan= (TextView) itemView.findViewById(R.id.tvKan);
            mYer= (TextView) itemView.findViewById(R.id.tvYer);

            mSilResmi=(ImageView) itemView.findViewById(R.id.imgSil);


            mSilResmi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("EMRE", "Silmeden önce position:"+tiklanilanOgeninPositionDegeri+" ElemanSayısı "+mDataList.size());
                    deleteItem(tiklanilanOgeninPositionDegeri,tiklanilanOgeId);
                    Log.d("EMRE", "Silmeden sonra position:"+tiklanilanOgeninPositionDegeri+" ElemanSayısı "+mDataList.size());
                }
            });





        }

        public void setData(WantedBlood tiklanilanKan, int position) {
            this.mKan.setText(tiklanilanKan.getFullName()+" ("+tiklanilanKan.getBlood()+")");
            this.mYer.setText(tiklanilanKan.getCity()+","+tiklanilanKan.getTown());
            this.tiklanilanOgeninPositionDegeri = position;
            this.tiklanilanOgeId = tiklanilanKan.getID();

        }
    }
}