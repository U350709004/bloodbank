package com.example.harun.bloodbank.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.harun.bloodbank.WantedBlood;

import java.util.ArrayList;
import java.util.List;

public class Database  extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "veritabani";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE = "wantedbloods";

    public static final String ROW_ID = "id";
    public static final String ROW_FULL_NAME = "fullname";
    private static final String ROW_PHONE_NUMBER = "phonenumber";
    private static final String ROW_CITY = "city";
    private static final String ROW_TOWN = "town";
    private static final String ROW_BLOOD = "blood";



    public Database(Context context) { super(context,DATABASE_NAME,null,DATABASE_VERSION); }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+ TABLE+ "("+ROW_ID+" INTEGER PRIMARY KEY, "+ROW_FULL_NAME+" TEXT NOT NULL, "+ROW_PHONE_NUMBER+" TEXT NOT NULL, "
        +ROW_CITY+" TEXT NOT NULL ,"+ ROW_TOWN+" TEXT NOT NULL, "+ROW_BLOOD+" TEXT NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE);
    }

    public void insertData(WantedBlood wb){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ROW_FULL_NAME,wb.getFullName());
        cv.put(ROW_PHONE_NUMBER, wb.getPhoneNumber());
        cv.put(ROW_CITY,wb.getCity());
        cv.put(ROW_TOWN,wb.getTown());
        cv.put(ROW_BLOOD, wb.getBlood());
        db.insert(TABLE,null,cv);
        db.close();
    }

    public List<WantedBlood> allFindingBloods(){
        List<WantedBlood> list = new ArrayList<WantedBlood>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = {ROW_FULL_NAME,ROW_PHONE_NUMBER,ROW_CITY,ROW_TOWN,ROW_BLOOD,ROW_ID};
        Cursor cursor = db.query(TABLE,cols,null,null,null,null,null);
        while(cursor.moveToNext()){
            WantedBlood wb = new WantedBlood(cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
                    );
            wb.setID(cursor.getInt(5));
            list.add(wb);
        }
        return list;
    }

    public void deletItem(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE,ROW_ID+"="+id,null);
        db.close();
    }
}
