package com.example.harun.bloodbank.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.harun.bloodbank.R;
import com.example.harun.bloodbank.WantedBlood;
import com.example.harun.bloodbank.adapters.MyFindingBloodAdapter;
import com.example.harun.bloodbank.database.Database;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfileFindsFlood extends Fragment {
    View v;
    ListView liste;
    RecyclerView recyclerView;
    List<WantedBlood> wantedBloodList = new ArrayList<WantedBlood>();

    public FragmentProfileFindsFlood() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.v = inflater.inflate(R.layout.fragment_fragment_profile_finds_flood, container, false);
        loadData();
        recyclerView= (RecyclerView) v.findViewById(R.id.recyclerview);

        MyFindingBloodAdapter emreAdapter=new MyFindingBloodAdapter(getContext(),wantedBloodList);
        recyclerView.setAdapter(emreAdapter);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        return  v;
    }

    public void loadData() {
        Database db = new Database(getActivity());
        wantedBloodList = db.allFindingBloods();
        //wantedBloodList.add(new WantedBlood("Yusuf İlker Oğuz","0525 323 42 56","İstanbul","Kadıköy","AB RH+"));
        //wantedBloodList.add(new WantedBlood("Kaan Burkay Kabakçı","0543 241 13 93","İstanbul","Kadıköy","0 RH+"));
        //wantedBloodList.add(new WantedBlood("Burhan Çakar","0564 565 23 35","İstanbul","Kadıköy","0 RH-"));


    }

}
